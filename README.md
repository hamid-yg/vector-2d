# vector-2d

## DESCRIPTION

Many Small exercices about 2d Vector in C++

## GOALS

- Canonical Form
- Operations between Vectors
- Operators overloading
- Error Handling with Exceptions

---

<div align="center">

<a href="https://gitlab.com/blacky-yg" target="_blank"><img src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/gitlab.svg" alt="gitlab.com" width="30"></a>

</div>